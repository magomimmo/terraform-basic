resource "kubernetes_service" "fleetman-mongodb" {
  metadata {
    name = "fleetman-mongodb"
  }
  spec {
    selector = {
      app = kubernetes_deployment.fleetman-mongodb.spec.0.template.0.metadata.0.labels.app
    }
    type = "ClusterIP"
    port {
      name        = "http"
      port        = 27017
      target_port = 27017
    }
  }
}

resource "kubernetes_service" "fleetman-queue" {
  metadata {
    name = "fleetman-queue"
  }
  spec {
    selector = {
      app = kubernetes_deployment.fleetman-queue.spec.0.template.0.metadata.0.labels.app
    }
    type = "ClusterIP"
    port {
      name        = "http"
      port        = 8161
      target_port = 8161
    }
    port {
      name        = "endpoint"
      port        = 61616
      target_port = 61616
    }
  }
}

resource "kubernetes_service" "fleetman-position-tracker" {
  metadata {
    name = "fleetman-position-tracker"
  }
  spec {
    selector = {
      app = kubernetes_deployment.fleetman-position-tracker.spec.0.template.0.metadata.0.labels.app
    }
    type = "ClusterIP"
    port {
      port        = 8080
      target_port = 8080
    }
  }
}

resource "kubernetes_service" "fleetman-api-gateway" {
  metadata {
    name = "fleetman-api-gateway"
  }
  spec {
    selector = {
      app = kubernetes_deployment.fleetman-api-gateway.spec.0.template.0.metadata.0.labels.app
    }
    type = "ClusterIP"
    port {
      port        = 8080
      target_port = 8080
    }
  }
}

resource "kubernetes_service" "fleetman-webapp" {
  metadata {
    name = "fleetman-webapp"
  }
  spec {
    selector = {
      app = kubernetes_deployment.fleetman-webapp.spec.0.template.0.metadata.0.labels.app
    }
    type = "NodePort"
    port {
      node_port   = 30080
      port        = 80
      target_port = 80
    }
  }
}
