resource "kubernetes_deployment" "fleetman-mongodb" {
  metadata {
    name = "mongodb"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "mongodb"
      }
    }
    template {
      metadata {
        labels = {
          app = "mongodb"
        }
      }
      spec {
        container {
          image = "mongo:3.6.5-jessie"
          name  = "mongodb"
          port {
            container_port = 27017
          }
          env {
            name  = "SPRING_PROFILES_ACTIVE"
            value = "production-microservice"
          }
        }
      }
    }
  }
}

resource "kubernetes_deployment" "fleetman-position-simulator" {
  metadata {
    name = "position-simulator"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "position-simulator"
      }
    }
    template {
      metadata {
        labels = {
          app = "position-simulator"
        }
      }
      spec {
        container {
          image = "richardchesterwood/k8s-fleetman-position-simulator:release2"
          name  = "position-simulator"
          env {
            name  = "SPRING_PROFILES_ACTIVE"
            value = "production-microservice"
          }
        }
      }
    }
  }
}

resource "kubernetes_deployment" "fleetman-queue" {
  metadata {
    name = "queue"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "queue"
      }
    }
    template {
      metadata {
        labels = {
          app = "queue"
        }
      }
      spec {
        container {
          image = "richardchesterwood/k8s-fleetman-queue:release2"
          name  = "queue"
          port {
            container_port = 8161
          }
          port {
            container_port = 61616
          }
        }
      }
    }
  }
}

resource "kubernetes_deployment" "fleetman-position-tracker" {
  metadata {
    name = "position-tracker"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "position-tracker"
      }
    }
    template {
      metadata {
        labels = {
          app = "position-tracker"
        }
      }
      spec {
        container {
          image = "richardchesterwood/k8s-fleetman-position-tracker:release3"
          name  = "position-tracker"
          port {
            container_port = 8080
          }
          env {
            name  = "SPRING_PROFILES_ACTIVE"
            value = "production-microservice"
          }
        }
      }
    }
  }
}

resource "kubernetes_deployment" "fleetman-api-gateway" {
  metadata {
    name = "api-gateway"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "api-gateway"
      }
    }
    template {
      metadata {
        labels = {
          app = "api-gateway"
        }
      }
      spec {
        container {
          image = "richardchesterwood/k8s-fleetman-api-gateway:release2"
          name  = "api-gateway"
          port {
            container_port = 8080
          }
          env {
            name  = "SPRING_PROFILES_ACTIVE"
            value = "production-microservice"
          }
        }
      }
    }
  }
}

resource "kubernetes_deployment" "fleetman-webapp" {
  metadata {
    name = "webapp"
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "webapp"
      }
    }
    template {
      metadata {
        labels = {
          app = "webapp"
        }
      }
      spec {
        container {
          image = "richardchesterwood/k8s-fleetman-webapp-angular:release2"
          name  = "webapp"
          port {
            container_port = 80
          }
          env {
            name  = "SPRING_PROFILES_ACTIVE"
            value = "production-microservice"
          }
        }
      }
    }
  }
}
